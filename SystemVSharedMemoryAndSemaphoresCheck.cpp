//===--- SystemVSharedMemoryAndSemaphoresCheck.cpp - clang-tidy
//--------------------------------------===//
//
//  checks that students don't call the system V functions
//  for shared memory or semaphores
//
//===----------------------------------------------------------------------===//

#include "GBRChecks.h"
#include <clang/AST/ASTContext.h>
#include <clang/ASTMatchers/ASTMatchFinder.h>

using namespace clang::ast_matchers;

namespace clang {
namespace tidy {
namespace gbr {

void SystemVSharedMemoryAndSemaphoresCheck::registerMatchers(
    MatchFinder *Finder) {
  Finder->addMatcher(
      callExpr(callee(functionDecl(hasAnyName("::shmget", "::shmat", "::shmdt",
                                              "::shmctl", "::semget", "::semop",
                                              "::semctl"))
                          .bind("func")))
          .bind("expr"),
      this);
}

void SystemVSharedMemoryAndSemaphoresCheck::check(
    const MatchFinder::MatchResult &Result) {
  const auto *Call = Result.Nodes.getNodeAs<CallExpr>("expr");
  const FunctionDecl *FuncDecl = Result.Nodes.getNodeAs<FunctionDecl>("func");
  diag(Call->getBeginLoc(),
       "Function %0 is obsolete. See man sem_overview or shm_overview.")
      << FuncDecl;
}

} // namespace gbr
} // namespace tidy
} // namespace clang
