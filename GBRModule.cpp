#include "GBRChecks.h"
#include <ClangTidy.h>
#include <ClangTidyModule.h>
#include <ClangTidyModuleRegistry.h>

using namespace clang::ast_matchers;

namespace clang {
namespace tidy {
namespace gbr {

class GBRModule : public ClangTidyModule {
public:
  void addCheckFactories(ClangTidyCheckFactories &CheckFactories) override {
    CheckFactories.registerCheck<NetdbCheck>("modernize-netdb");
    CheckFactories.registerCheck<SignalCheck>("modernize-signal");
    CheckFactories.registerCheck<WRSRCheck>("bugprone-netio");
    CheckFactories.registerCheck<RequiredFunctionsList>("bugprone-testing-list");
    CheckFactories.registerCheck<RequiredFunctionsZins>("bugprone-testing-zins");
    CheckFactories.registerCheck<KillCheck>("bugprone-forbidden-function");
    CheckFactories.registerCheck<SystemVSharedMemoryAndSemaphoresCheck>("modernize-shm-sem");
    CheckFactories.registerCheck<ProcTableatoiCheck>("modernize-atoi");
    CheckFactories.registerCheck<RequiredFunctionsStack>("bugprone-testing-stack");
    CheckFactories.registerCheck<RequiredFunctionsRingbuf>("bugprone-testing-ringbuf");
    CheckFactories.registerCheck<RequiredFunctionsProcList>("bugprone-testing-proclist");
  }
};

// Register the GBRTidyModule using this statically initialized variable.
static ClangTidyModuleRegistry::Add<GBRModule> X("gbr-module",
                                                 "Add gbr checks.");

} // namespace gbr

// This anchor is used to force the linker to link in the generated object file
// and thus register the GBRModule.
volatile int GBRModuleAnchorSource = 0;

} // namespace tidy
} // namespace clang
