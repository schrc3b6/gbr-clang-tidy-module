//===--- requiredFunctionsRingbuf.cpp - clang-tidy
//---------------------------===//
//
// required Functions for task 1.5 Ringbuffer
//
//===----------------------------------------------------------------------===//

#include "GBRChecks.h"
#include <clang/AST/ASTContext.h>
#include <clang/ASTMatchers/ASTMatchFinder.h>
#include <llvm/Support/raw_ostream.h>

using namespace clang::ast_matchers;

namespace clang {
namespace tidy {
namespace gbr {

void RequiredFunctionsRingbuf::registerMatchers(MatchFinder *Finder) {
  Finder->addMatcher(
      functionDecl(hasName("main"),
                   unless(hasDescendant(callExpr(
                       callee(functionDecl(hasName("ringbuf_create")))))))
          .bind("ringbuf_create"),
      this);
  Finder->addMatcher(
      functionDecl(hasName("main"),
                   unless(hasDescendant(callExpr(
                       callee(functionDecl(hasName("ringbuf_delete")))))))
          .bind("ringbuf_delete"),
      this);
  Finder->addMatcher(
      functionDecl(hasName("main"),
                   unless(hasDescendant(callExpr(
                       callee(functionDecl(hasName("ringbuf_append")))))))
          .bind("ringbuf_append"),
      this);
  Finder->addMatcher(functionDecl(hasName("main"),
                                  unless(hasDescendant(callExpr(callee(
                                      functionDecl(hasName("ringbuf_pop")))))))
                         .bind("ringbuf_pop"),
                     this);
  Finder->addMatcher(
      functionDecl(hasName("main"),
                   unless(hasDescendant(callExpr(callee(
                       functionDecl(hasName("ringbuf_pop_delimited")))))))
          .bind("ringbuf_pop_delimited"),
      this);
}

void RequiredFunctionsRingbuf::check(const MatchFinder::MatchResult &Result) {
  const auto *createFunc =
      Result.Nodes.getNodeAs<FunctionDecl>("ringbuf_create");
  if (createFunc)
    diag(createFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "ringbuf_create";

  const FunctionDecl *deleteFunc =
      Result.Nodes.getNodeAs<FunctionDecl>("ringbuf_delete");
  if (deleteFunc)
    diag(deleteFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "ringbuf_delete";

  const FunctionDecl *appendFunc =
      Result.Nodes.getNodeAs<FunctionDecl>("ringbuf_append");
  if (appendFunc)
    diag(appendFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "ringbuf_append";

  const FunctionDecl *popFunc =
      Result.Nodes.getNodeAs<FunctionDecl>("ringbuf_pop");
  if (popFunc)
    diag(popFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "ringbuf_pop";

  const FunctionDecl *popDelimitedFunc =
      Result.Nodes.getNodeAs<FunctionDecl>("ringbuf_pop_delimited");
  if (popDelimitedFunc)
    diag(popDelimitedFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "ringbuf_pop_delimited";
}

} // namespace gbr
} // namespace tidy
} // namespace clang
