//===--- requiredFunctionsZins.cpp - clang-tidy ---------------------------===//
//
// required Functions for PdP single linked list task
//
//===----------------------------------------------------------------------===//

#include "GBRChecks.h"
#include <clang/AST/ASTContext.h>
#include <clang/ASTMatchers/ASTMatchFinder.h>
#include <llvm/Support/raw_ostream.h>

using namespace clang::ast_matchers;

namespace clang {
namespace tidy {
namespace gbr {

void RequiredFunctionsZins::registerMatchers(MatchFinder *Finder) {
  // FIXME: Add matchers.

  Finder->addMatcher(
      functionDecl(hasName("erzeuge_kapital_liste"), unless(hasDescendant(callExpr(callee(
                                        functionDecl(hasName("berechne")))))))
          .bind("berechne"),
      this);
  Finder->addMatcher(
      functionDecl(hasName("main"), unless(hasDescendant(callExpr(callee(
                                        functionDecl(hasName("erzeuge_kapital_liste")))))))
          .bind("erzeuge_kapital_liste"),
      this);
  Finder->addMatcher(
      functionDecl(hasName("main"), unless(hasDescendant(callExpr(callee(
                                        functionDecl(hasName("pretty_print")))))))
          .bind("pretty_print"),
      this);
}

void RequiredFunctionsZins::check(const MatchFinder::MatchResult &Result) {
  // FIXME: Add callback implementation.

  const auto *kapitalFunc = Result.Nodes.getNodeAs<FunctionDecl>("erzeuge_kapital_liste");
  if (kapitalFunc)
    diag(kapitalFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "erzeuge_kapital_liste";

  const FunctionDecl *printFunc =
      Result.Nodes.getNodeAs<FunctionDecl>("pretty_print");
  if (printFunc)
    diag(printFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "pretty_print";

  const auto *berechneFunc = Result.Nodes.getNodeAs<FunctionDecl>("berechne");
  if (berechneFunc)
    diag(berechneFunc->getBeginLoc(),
         "In the erzeuge_kapital_liste function, you should call the %0 function."
         "It seems the call to %0 are missing in the erzeuge_kapital_liste function.")
        << "berechne";

}

} // namespace gbr
} // namespace tidy
} // namespace clang
