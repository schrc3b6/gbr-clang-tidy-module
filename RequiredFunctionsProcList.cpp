//===--- requiredFunctionsProcList.cpp - clang-tidy
//---------------------------===//
//
// required Functions for task 1.5 dl_proc_listfer
//
//===----------------------------------------------------------------------===//

#include "GBRChecks.h"
#include <clang/AST/ASTContext.h>
#include <clang/ASTMatchers/ASTMatchFinder.h>
#include <llvm/Support/raw_ostream.h>

using namespace clang::ast_matchers;

namespace clang {
namespace tidy {
namespace gbr {

void RequiredFunctionsProcList::registerMatchers(MatchFinder *Finder) {
  Finder->addMatcher(
      functionDecl(hasName("main"),
                   unless(hasDescendant(callExpr(
                       callee(functionDecl(hasName("dl_proc_list_create")))))))
          .bind("dl_proc_list_create"),
      this);
  Finder->addMatcher(
      functionDecl(hasName("main"),
                   unless(hasDescendant(callExpr(
                       callee(functionDecl(hasName("dl_proc_list_free")))))))
          .bind("dl_proc_list_free"),
      this);
  Finder->addMatcher(
      functionDecl(hasName("main"),
                   unless(hasDescendant(callExpr(
                       callee(functionDecl(hasName("dl_proc_list_insert")))))))
          .bind("dl_proc_list_insert"),
      this);
  Finder->addMatcher(
      functionDecl(hasName("main"),
                   unless(hasDescendant(callExpr(
                       callee(functionDecl(hasName("dl_proc_list_remove")))))))
          .bind("dl_proc_list_remove"),
      this);
  Finder->addMatcher(
      functionDecl(hasName("main"),
                   unless(hasDescendant(callExpr(
                       callee(functionDecl(hasName("dl_proc_list_get")))))))
          .bind("dl_proc_list_get"),
      this);
}

void RequiredFunctionsProcList::check(const MatchFinder::MatchResult &Result) {
  const auto *createFunc =
      Result.Nodes.getNodeAs<FunctionDecl>("dl_proc_list_create");
  if (createFunc)
    diag(createFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "dl_proc_list_create";

  const FunctionDecl *freeFunc =
      Result.Nodes.getNodeAs<FunctionDecl>("dl_proc_list_free");
  if (freeFunc)
    diag(freeFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "dl_proc_list_free";

  const FunctionDecl *insertFunc =
      Result.Nodes.getNodeAs<FunctionDecl>("dl_proc_list_insert");
  if (insertFunc)
    diag(insertFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "dl_proc_list_insert";

  const FunctionDecl *removeFunc =
      Result.Nodes.getNodeAs<FunctionDecl>("dl_proc_list_remove");
  if (removeFunc)
    diag(removeFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "dl_proc_list_remove";

  const FunctionDecl *getFunc =
      Result.Nodes.getNodeAs<FunctionDecl>("dl_proc_list_get");
  if (getFunc)
    diag(getFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "dl_proc_list_get";
}

} // namespace gbr
} // namespace tidy
} // namespace clang
