//===--- requiredFunctionsList.cpp - clang-tidy ---------------------------===//
//
// required Functions for PdP single linked list task
//
//===----------------------------------------------------------------------===//

#include "GBRChecks.h"
#include <clang/AST/ASTContext.h>
#include <clang/ASTMatchers/ASTMatchFinder.h>
#include <llvm/Support/raw_ostream.h>

using namespace clang::ast_matchers;

namespace clang {
namespace tidy {
namespace gbr {

void RequiredFunctionsList::registerMatchers(MatchFinder *Finder) {
  // FIXME: Add matchers.

  Finder->addMatcher(
      functionDecl(hasName("main"), unless(hasDescendant(callExpr(callee(
                                        functionDecl(hasName("insert")))))))
          .bind("insert"),
      this);
  Finder->addMatcher(
      functionDecl(hasName("main"), unless(hasDescendant(callExpr(callee(
                                        functionDecl(hasName("delete_all")))))))
          .bind("delete_all"),
      this);
  Finder->addMatcher(
      functionDecl(hasName("main"), unless(hasDescendant(callExpr(callee(
                                        functionDecl(hasName("print_list")))))))
          .bind("print_list"),
      this);
  Finder->addMatcher(functionDecl(hasName("main"),
                                  unless(hasDescendant(callExpr(callee(
                                      functionDecl(hasName("position_of")))))))
                         .bind("position_of"),
                     this);
  Finder->addMatcher(functionDecl(hasName("main"),
                                  unless(hasDescendant(callExpr(callee(
                                      functionDecl(hasName("filter_even")))))))
                         .bind("filter_even"),
                     this);
}

void RequiredFunctionsList::check(const MatchFinder::MatchResult &Result) {
  // FIXME: Add callback implementation.

  const auto *insertFunc = Result.Nodes.getNodeAs<FunctionDecl>("insert");
  if (insertFunc)
    diag(insertFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "insert";

  const FunctionDecl *delete_allFunc =
      Result.Nodes.getNodeAs<FunctionDecl>("delete_all");
  if (delete_allFunc)
    diag(delete_allFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "delete_all";

  const FunctionDecl *print_listFunc =
      Result.Nodes.getNodeAs<FunctionDecl>("print_list");
  if (print_listFunc)
    diag(print_listFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "print_list";

  const FunctionDecl *position_ofFunc =
      Result.Nodes.getNodeAs<FunctionDecl>("position_of");
  if (position_ofFunc)
    diag(position_ofFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "position_of";

  const FunctionDecl *delete_posFunc =
      Result.Nodes.getNodeAs<FunctionDecl>("delete_pos");
  if (delete_posFunc)
    diag(delete_posFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "delete_pos";

  const FunctionDecl *filter_evenFunc =
      Result.Nodes.getNodeAs<FunctionDecl>("filter_even");
  if (filter_evenFunc)
    diag(filter_evenFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "filter_even";
}

} // namespace gbr
} // namespace tidy
} // namespace clang
