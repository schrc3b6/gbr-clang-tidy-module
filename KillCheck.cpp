//===--- KillCheck.cpp - clang-tidy --------------------------------------===//
//
//  checks that students don't call functions like kill
//  which interfere with testing
//
//===----------------------------------------------------------------------===//

#include "GBRChecks.h"
#include <clang/AST/ASTContext.h>
#include <clang/ASTMatchers/ASTMatchFinder.h>

using namespace clang::ast_matchers;

namespace clang {
namespace tidy {
namespace gbr {

void KillCheck::registerMatchers(MatchFinder *Finder) {
  Finder->addMatcher(
      callExpr(callee(functionDecl(hasAnyName("::_exit", "::_Exit", "::kill",
                                              "::pidfd_send_signal", "::tkill",
                                              "::tgkill", "::killpg"))
                          .bind("func")))
          .bind("expr"),
      this);
}

void KillCheck::check(const MatchFinder::MatchResult &Result) {
  const auto *Call = Result.Nodes.getNodeAs<CallExpr>("expr");
  const FunctionDecl *FuncDecl = Result.Nodes.getNodeAs<FunctionDecl>("func");
  diag(Call->getBeginLoc(),
       "Function %0 should not be used because it interferes with the tests.")
      << FuncDecl;
}

} // namespace gbr
} // namespace tidy
} // namespace clang
