//===--- requiredFunctionsStack.cpp - clang-tidy
//---------------------------===//
//
// required Functions for task 0.1 Stack
//
//===----------------------------------------------------------------------===//

#include "GBRChecks.h"
#include <clang/AST/ASTContext.h>
#include <clang/ASTMatchers/ASTMatchFinder.h>
#include <llvm/Support/raw_ostream.h>

using namespace clang::ast_matchers;

namespace clang {
namespace tidy {
namespace gbr {

void RequiredFunctionsStack::registerMatchers(MatchFinder *Finder) {
  Finder->addMatcher(functionDecl(hasName("main"),
                                  unless(hasDescendant(callExpr(callee(
                                      functionDecl(hasName("stack_create")))))))
                         .bind("stack_create"),
                     this);
  Finder->addMatcher(functionDecl(hasName("main"),
                                  unless(hasDescendant(callExpr(callee(
                                      functionDecl(hasName("stack_delete")))))))
                         .bind("stack_delete"),
                     this);
  Finder->addMatcher(functionDecl(hasName("main"),
                                  unless(hasDescendant(callExpr(callee(
                                      functionDecl(hasName("stack_length")))))))
                         .bind("stack_length"),
                     this);
  Finder->addMatcher(
      functionDecl(hasName("main"), unless(hasDescendant(callExpr(callee(
                                        functionDecl(hasName("stack_push")))))))
          .bind("stack_push"),
      this);
  Finder->addMatcher(
      functionDecl(hasName("main"), unless(hasDescendant(callExpr(callee(
                                        functionDecl(hasName("stack_pop")))))))
          .bind("stack_pop"),
      this);
}

void RequiredFunctionsStack::check(const MatchFinder::MatchResult &Result) {
  const auto *createFunc = Result.Nodes.getNodeAs<FunctionDecl>("stack_create");
  if (createFunc)
    diag(createFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "stack_create";

  const FunctionDecl *deleteFunc =
      Result.Nodes.getNodeAs<FunctionDecl>("stack_delete");
  if (deleteFunc)
    diag(deleteFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "stack_delete";

  const FunctionDecl *lengthFunc =
      Result.Nodes.getNodeAs<FunctionDecl>("stack_length");
  if (lengthFunc)
    diag(lengthFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "stack_length";

  const FunctionDecl *pushFunc =
      Result.Nodes.getNodeAs<FunctionDecl>("stack_push");
  if (pushFunc)
    diag(pushFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "stack_push";

  const FunctionDecl *popFunc =
      Result.Nodes.getNodeAs<FunctionDecl>("stack_pop");
  if (popFunc)
    diag(popFunc->getBeginLoc(),
         "In the main function, you should test all required functions."
         "It seems calls to %0 are missing in the main function.")
        << "stack_pop";
}

} // namespace gbr
} // namespace tidy
} // namespace clang
