//===--- ProcTableatoiCheck.cpp - clang-tidy
//--------------------------------------===//
//
//  checks that students don't call the atoi, atol, atoll, atof functions
//  for converting strings to numbers
//
//===----------------------------------------------------------------------===//

#include "GBRChecks.h"
#include <clang/AST/ASTContext.h>
#include <clang/ASTMatchers/ASTMatchFinder.h>

using namespace clang::ast_matchers;

namespace clang {
namespace tidy {
namespace gbr {

void ProcTableatoiCheck::registerMatchers(MatchFinder *Finder) {
  Finder->addMatcher(
      callExpr(callee(functionDecl(
                          hasAnyName("::atoi", "::atol", "::atoll", "::atof"))
                          .bind("func")))
          .bind("expr"),
      this);
}

void ProcTableatoiCheck::check(const MatchFinder::MatchResult &Result) {
  const auto *Call = Result.Nodes.getNodeAs<CallExpr>("expr");
  const FunctionDecl *FuncDecl = Result.Nodes.getNodeAs<FunctionDecl>("func");
  diag(Call->getBeginLoc(),
       "Function %0 is obsolete. See man %0 for recommended function.")
      << FuncDecl;
}

} // namespace gbr
} // namespace tidy
} // namespace clang
